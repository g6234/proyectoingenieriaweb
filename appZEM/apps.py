from django.apps import AppConfig


class AppzemConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'appZEM'
