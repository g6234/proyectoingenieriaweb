from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views import generic
from .forms import PedidoForm, UserCreationForm, ComponenteForm, ProductoForm, ClienteForm, PedidoFilterForm, ProductoFilterForm, ComponenteFilterForm, ClienteFilterForm
from django.shortcuts import get_object_or_404, get_list_or_404
from django.urls import reverse
from django.views import View
from django.views.generic import DetailView, ListView
from .models import Pedido, Producto, Componente, Cliente
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django_filters.views import FilterView
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
import json


# Formulario registro

class SignUpView(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy("login")
    template_name = "registration/signup.html"


# Borrar pedido

@login_required
def PedidoDelete(request, pk):
    pedido = get_object_or_404(Pedido, id=pk)
    pedido.delete()
    pedido = Pedido.objects.all()
    return redirect(to='pedidos_list')


# Borrar producto

@login_required
def ProductoDelete(request, pk):
    producto = get_object_or_404(Producto, id=pk)
    producto.delete()
    producto = Producto.objects.all()
    return redirect(to='productos_list')


# Borrar cliente

@login_required
def ClienteDelete(request, pk):
    cliente = get_object_or_404(Cliente, id=pk)
    cliente.delete()
    cliente = Cliente.objects.all()
    return redirect(to='clientes_list')


# Borrar componente

@login_required
def ComponenteDelete(request, pk):
    componente = get_object_or_404(Componente, id=pk)
    componente.delete()
    componente = Componente.objects.all()
    return redirect(to='componentes_list')


# buscar pedido mediante codigo

@method_decorator(csrf_exempt, name='dispatch')
def pedidoFind(request):
    try:
        cod_ref_pedido = json.load(request)['codigo']
        Pedido.objects.get(cod_ref_pedido=cod_ref_pedido)
        data = {
            'existe': True
        }
        return JsonResponse(data)
    except ObjectDoesNotExist:
        data = {
            'existe': False
        }
        return JsonResponse(data)


# buscar producto mediante codigo

@method_decorator(csrf_exempt, name='dispatch')
def productoFind(request):
    try:
        cod_ref_producto = json.load(request)['codigo']
        Producto.objects.get(cod_ref_producto=cod_ref_producto)
        data = {
            'existe': True
        }
        return JsonResponse(data)
    except ObjectDoesNotExist:
        data = {
            'existe': False
        }
        return JsonResponse(data)


# buscar componente mediante codigo

@method_decorator(csrf_exempt, name='dispatch')
def componenteFind(request):
    try:
        cod_ref_componente = json.load(request)['codigo']
        Componente.objects.get(cod_ref_componente=cod_ref_componente)
        data = {
            'existe': True
        }
        return JsonResponse(data)
    except ObjectDoesNotExist:
        data = {
            'existe': False
        }
        return JsonResponse(data)


# Clase que devuelve los datos de un pedido

class PedidoDetailView(LoginRequiredMixin, DetailView):
    model = Pedido
    template_name = "pedido_detail.html"

    def get_context_data(self, **kwargs):
        context = super(PedidoDetailView,
                        self).get_context_data(**kwargs)
        context["titulo"] = "Detalles del pedido"
        return context


# Clase que devuelve los datos de un producto

class ProductoDetailView(DetailView):
    model = Producto
    template_name = "producto_detail.html"

    def get_context_data(self, **kwargs):
        context = super(ProductoDetailView,
                        self).get_context_data(**kwargs)
        context["titulo"] = "Detalles del producto"
        return context


# Clase que devuelve los datos de un cliente

class ClienteDetailView(DetailView):
    model = Cliente
    template_name = "cliente_detail.html"

    def get_context_data(self, **kwargs):
        context = super(ClienteDetailView,
                        self).get_context_data(**kwargs)
        context["titulo"] = "Detalles del cliente"
        return context


# Clase que devuleve los datos de un componente

class ComponenteDetailView(DetailView):
    model = Componente
    template_name = "componente_detail.html"

    def get_context_data(self, **kwargs):
        context = super(ComponenteDetailView,
                        self).get_context_data(**kwargs)
        context["titulo"] = "Detalles del componente"
        return context


# Clase que lista los componentes

class SearchResultsComponenteListView(LoginRequiredMixin, FilterView):
    model = Componente
    context_object_name = 'contenido'
    template_name = 'componentes_list.html'
    filterset_class = ComponenteFilterForm
    paginate_by = 5

    def get_context_data(self, **kwargs):

        context = super(SearchResultsComponenteListView,
                        self).get_context_data(**kwargs)
        context['titulo'] = 'Lista de componentes'
        return context


# Clase que lista los clientes

class SearchResultsClienteListView(LoginRequiredMixin, FilterView):
    model = Cliente
    context_object_name = 'contenido'
    template_name = 'clientes_list.html'
    filterset_class = ClienteFilterForm
    paginate_by = 5

    def get_context_data(self, **kwargs):

        context = super(SearchResultsClienteListView,
                        self).get_context_data(**kwargs)
        context['titulo'] = 'Lista de clientes'
        return context


# Clase que lista los productos

class SearchResultsProductoListView(LoginRequiredMixin, FilterView):
    model = Producto
    context_object_name = 'contenido'
    template_name = 'productos_list.html'
    filterset_class = ProductoFilterForm
    paginate_by = 5

    def get_context_data(self, **kwargs):

        context = super(SearchResultsProductoListView,
                        self).get_context_data(**kwargs)
        context['titulo'] = 'Lista de productos'
        return context


# Clase que lista los pedidos

class SearchResultsPedidoListView(LoginRequiredMixin, FilterView):
    model = Pedido
    context_object_name = 'contenido'
    template_name = 'pedidos_list.html'
    filterset_class = PedidoFilterForm
    paginate_by = 5

    def get_context_data(self, **kwargs):

        context = super(SearchResultsPedidoListView,
                        self).get_context_data(**kwargs)
        context['titulo'] = 'Lista de pedidos'
        return context


# Clase donde se crea o edita los pedidos

class PedidoView(LoginRequiredMixin, View):
    # Llamada para mostrar la página con el formulario de creación al usuario
    def get(self, request, *args, **kwargs):
        if kwargs.__len__() != 0:
            pedido = Pedido.objects.get(id=kwargs['pk'])
            form = PedidoForm(instance=pedido)
            context = {
                'form': form,
                'titulo': 'Editar Pedido',
                'pedido_pk': pedido.pk
            }
        else:
            form = PedidoForm()
            context = {
                'form': form,
                'titulo': 'Nuevo Pedido'
            }
        return render(request, 'pedido_view.html', context)

    # Llamada para procesar la creación del pedido

    def post(self, request, *args, **kwargs):
        if kwargs.__len__() != 0:
            pedido = Pedido.objects.get(id=kwargs['pk'])
            form = PedidoForm(request.POST, instance=pedido)
        else:
            form = PedidoForm(request.POST)
        if form.is_valid():  # is_valid() deja los datos validados en el atributo cleaned_data
            # pedido = Pedido()

            # cod_ref_pedido =form.cleaned_data['cod_ref_pedido']
            # cantidad_producto = form.cleaned_data['cantidad_producto']
            # comentarios = form.cleaned_data['comentarios']

            # producto = form.cleaned_data['producto']
            # cliente = form.cleaned_data['cliente']
            # pedido.save()

            form.save()  # Abreviación de lo anterior

            # Volvemos a la lista de pedidos
            return redirect('home')
        # Si los datos no son válidos, mostramos el formulario nuevamente indicando los errores
        return render(request, 'pedido_view.html', {'form': form})


# Clase donde se crea o edita los componentes

class ComponenteView(LoginRequiredMixin, View):
    # Llamada para mostrar la página con el formulario de creación al usuario
    def get(self, request, *args, **kwargs):
        if kwargs.__len__() != 0:
            componente = Componente.objects.get(id=kwargs['pk'])
            form = ComponenteForm(instance=componente)
            context = {
                'form': form,
                'titulo': 'Editar Componente',
                'componente_pk': componente.pk
            }
        else:
            form = ComponenteForm()
            context = {
                'form': form,
                'titulo': 'Nuevo Componente'
            }
        return render(request, 'componente_view.html', context)

    # Llamada para procesar la creación del componente

    def post(self, request, *args, **kwargs):
        if kwargs.__len__() != 0:
            componente = Componente.objects.get(id=kwargs['pk'])
            form = ComponenteForm(request.POST, instance=componente)
        else:
            form = ComponenteForm(request.POST)
        if form.is_valid():  # is_valid() deja los datos validados en el atributo cleaned_data
            # componente = Componente()

            # cod_ref_componente =form.cleaned_data['cod_ref_componente']
            # nombre_componente = form.cleaned_data['nombre_componente']
            # modelo = form.cleaned_data['modelo']

            # marca = form.cleaned_data['marca']
            # componente.save()

            form.save()  # Abreviación de lo anterior

            # Volvemos a la lista de componentes
            return redirect('componentes_list')
        # Si los datos no son válidos, mostramos el formulario nuevamente indicando los errores
        return render(request, 'componente_view.html', {'form': form})


# Clase donde se crea o edita los productos

class ProductoView(View):
    # Llamada para mostrar la página con el formulario de creación al usuario
    def get(self, request, *args, **kwargs):
        if kwargs.__len__() != 0:
            producto = Producto.objects.get(id=kwargs['pk'])
            form = ProductoForm(instance=producto)
            context = {
                'form': form,
                'titulo': 'Editar Producto',
                'producto_pk': producto.pk
            }
        else:
            form = ProductoForm()
            context = {
                'form': form,
                'titulo': 'Nuevo Producto'
            }
        return render(request, 'producto_view.html', context)

    # Llamada para procesar la creación del producto

    def post(self, request, *args, **kwargs):
        if kwargs.__len__() != 0:
            producto = Producto.objects.get(id=kwargs['pk'])
            form = ProductoForm(request.POST, instance=producto)
        else:
            form = ProductoForm(request.POST)
        if form.is_valid():  # is_valid() deja los datos validados en el atributo cleaned_data
            # componente = Componente()

            # cod_ref_componente =form.cleaned_data['cod_ref_componente']
            # nombre_componente = form.cleaned_data['nombre_componente']
            # modelo = form.cleaned_data['modelo']

            # marca = form.cleaned_data['marca']
            # componente.save()

            form.save()  # Abreviación de lo anterior

            # Volvemos a la lista de componentes
            return redirect('productos_list')
        # Si los datos no son válidos, mostramos el formulario nuevamente indicando los errores
        return render(request, 'producto_view.html', {'form': form})


# Clase donde se crea o edita los clientes

class ClienteView(LoginRequiredMixin, View):
    # Llamada para mostrar la página con el formulario de creación al usuario
    def get(self, request, *args, **kwargs):
        if kwargs.__len__() != 0:
            cliente = Cliente.objects.get(id=kwargs['pk'])
            form = ClienteForm(instance=cliente)
            context = {
                'form': form,
                'titulo': 'Editar Cliente',
                'cliente_pk': cliente.pk
            }
        else:
            form = ClienteForm()
            context = {
                'form': form,
                'titulo': 'Nuevo Cliente'
            }
        return render(request, 'cliente_view.html', context)

    # Llamada para procesar la creación del cliente
    def post(self, request, *args, **kwargs):
        if kwargs.__len__() != 0:
            cliente = Cliente.objects.get(id=kwargs['pk'])
            form = ClienteForm(request.POST, instance=cliente)
        else:
            form = ClienteForm(request.POST)
        if form.is_valid():  # is_valid() deja los datos validados en el atributo cleaned_data
            # cliente = Cliente()

            # cif =form.cleaned_data['cif']
            # nombre_cliente = form.cleaned_data['nombre_cliente']
            # direccion = form.cleaned_data['direccion']

            # email = form.cleaned_data['email']
            # telefono = form.cleaned_data['telefono']
            # url = form.cleaned_data['url']

            # cliente.save()

            form.save()  # Abreviación de lo anterior

            # Volvemos a la lista de clientes
            return redirect('clientes_list')
        # Si los datos no son válidos, mostramos el formulario nuevamente indicando los errores
        return render(request, 'cliente_view.html', {'form': form})
