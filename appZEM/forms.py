from django import forms
from django.forms import EmailField

from django.utils.translation import gettext_lazy as _

from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

from .models import Cliente, Componente, Pedido, Producto

import django_filters


class UserCreationForm(UserCreationForm):
    email = EmailField(label=_("Email address"), required=True,
                       help_text=_("Required."))

    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
        return user


class PedidoForm(forms.ModelForm):
    class Meta:
        model = Pedido
        fields = ['cod_ref_pedido', 'cantidad_producto',
                  'comentarios', 'producto', 'cliente']


class PedidoFilterForm(django_filters.FilterSet):
    class Meta:
        model = Pedido
        fields = ['cod_ref_pedido', 'producto', 'cliente']


class ComponenteForm(forms.ModelForm):
    class Meta:
        model = Componente
        fields = ['cod_ref_componente', 'nombre_componente', 'modelo', 'marca']


class ComponenteFilterForm(django_filters.FilterSet):
    class Meta:
        model = Componente
        fields = ['cod_ref_componente', 'nombre_componente', 'modelo', 'marca']


class ProductoForm(forms.ModelForm):
    class Meta:
        model = Producto
        fields = ['cod_ref_producto', 'nombre_producto',
                  'categoria', 'precio', 'descripcion', 'componentes']


class ProductoFilterForm(django_filters.FilterSet):
    class Meta:
        model = Producto
        fields = ['cod_ref_producto', 'nombre_producto', 'categoria']


class ClienteForm(forms.ModelForm):
    class Meta:
        model = Cliente
        fields = ['cif', 'nombre_cliente',
                  'telefono', 'email', 'direccion', 'url']


class ClienteFilterForm(django_filters.FilterSet):
    class Meta:
        model = Cliente
        fields = ['cif', 'nombre_cliente']
