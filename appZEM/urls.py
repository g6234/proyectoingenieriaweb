from django.urls import path, include
from . import views
from .views import SignUpView


urlpatterns = [
    path('', include("django.contrib.auth.urls")),
    path('', views.SearchResultsPedidoListView.as_view(), name='home'),
    path("signup/", SignUpView.as_view(), name="signup"),
    path('pedidos/<int:pk>/', views.PedidoDetailView.as_view(), name='pedido_detail'),
    path('pedidos/<int:pk>/update', views.PedidoView.as_view(), name='pedido_update'),
    path('pedidos/create/', views.PedidoView.as_view(), name='pedido_create'),
    path('pedidos/<int:pk>/delete', views.PedidoDelete, name='pedido_delete'),
    path('pedidos/find/', views.pedidoFind, name='pedido_find'),
    path('Listar_Pedidos', views.SearchResultsPedidoListView.as_view(), name='pedidos_list'),
    path('productos/<int:pk>/', views.ProductoDetailView.as_view(), name='producto_detail'),
    path('productos/<int:pk>/update', views.ProductoView.as_view(), name='producto_update'),
    path('productos/create/', views.ProductoView.as_view(), name='producto_create'),
    path('productos/<int:pk>/delete', views.ProductoDelete, name='producto_delete'),
    path('producto/find/', views.productoFind, name='producto_find'),
    path('Listar_Productos', views.SearchResultsProductoListView.as_view(), name='productos_list'),
    path('clientes/<int:pk>/', views.ClienteDetailView.as_view(), name='cliente_detail'),
    path('clientes/<int:pk>/update', views.ClienteView.as_view(), name='cliente_update'),
    path('clientes/create/', views.ClienteView.as_view(), name='cliente_create'),
    path('clientes/<int:pk>/delete', views.ClienteDelete, name='cliente_delete'),
    path('Listar_Clientes', views.SearchResultsClienteListView.as_view(), name='clientes_list'),
    path('componentes/<int:pk>/', views.ComponenteDetailView.as_view(), name='componente_detail'),    
    path('componentes/<int:pk>/update', views.ComponenteView.as_view(), name='componente_update'),
    path('componentes/create/', views.ComponenteView.as_view(), name='componente_create'),
    path('componentes/<int:pk>/delete', views.ComponenteDelete, name='componente_delete'),
    path('componente/find/', views.componenteFind, name='componente_find'),   
    path('Listar_Componentes', views.SearchResultsComponenteListView.as_view(), name='componentes_list'),
      
]
