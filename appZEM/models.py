from django.db import models
from django.utils import timezone

# Create your models here.


class Componente(models.Model):

    cod_ref_componente = models.CharField(
        max_length=12, unique=True, null=False)
    nombre_componente = models.CharField(max_length=30, unique=True, null=True)
    modelo = models.CharField(max_length=30, unique=True, null=False)
    marca = models.CharField(max_length=30, null=False)

    def __str__(self):
        return self.nombre_componente


class Producto(models.Model):

    cod_ref_producto = models.CharField(max_length=12, unique=True, null=False)
    nombre_producto = models.CharField(max_length=30, unique=True, null=False)
    precio = models.DecimalField(max_digits=10, decimal_places=2, null=False)
    categoria = models.CharField(max_length=30, unique=False, null=False)
    descripcion = models.CharField(max_length=300, unique=False, null=False)

    componentes = models.ManyToManyField(Componente)

    def __str__(self):
        return self.nombre_producto


class Cliente(models.Model):

    cif = models.CharField(max_length=9, unique=True, null=False)
    nombre_cliente = models.CharField(max_length=30, unique=True, null=False)
    direccion = models.CharField(max_length=150, unique=True, null=False)
    email = models.EmailField(max_length=50, unique=True, null=False)
    telefono = models.IntegerField(unique=True, null=False)
    url = models.CharField(max_length=150, unique=True, null=True)

    def __str__(self):
        return self.nombre_cliente


class Pedido(models.Model):

    cod_ref_pedido = models.CharField(max_length=12, unique=True, null=False)
    cantidad_producto = models.IntegerField(unique=False, null=False)
    comentarios = models.CharField(max_length=300, null=True)
    fecha = models.DateTimeField(default=timezone.now)
    precio_total = models.DecimalField(
        max_digits=10, decimal_places=2, default=0.0)

    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)

    def __str__(self):
        return self.cod_ref_pedido

    @property
    def suma_total(self):
        return (self.cantidad_producto * int(self.producto.precio))

    def save(self):
        self.precio_total = self.suma_total
        super(Pedido, self).save()
