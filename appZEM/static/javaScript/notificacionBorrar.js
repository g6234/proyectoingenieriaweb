
var modal = document.querySelector("#modal");
var modalOverlay = document.querySelector("#modal-overlay");
var botonesCerrar = document.querySelectorAll("#close-button");
var botonesBorrar = document.querySelectorAll(".delete");

for (var boton of botonesBorrar) {
    boton.addEventListener("click", ventanaBorrar)
}

for (var boton of botonesCerrar) {

    boton.addEventListener("click", ventanaCerrar)
}
function ventanaBorrar(event) {
    event.preventDefault();
    modal.classList.toggle("closed");
    modalOverlay.classList.toggle("closed");
}
function ventanaCerrar(event) {
    modal.classList.toggle("closed");
    modalOverlay.classList.toggle("closed");
}
