var cod_ref;
if (document.location.pathname.includes("producto")) {
    cod_ref = document.getElementsByName("cod_ref_producto")[0];
}
else {
    if (document.location.pathname.includes("pedido")) {
        cod_ref = document.getElementsByName("cod_ref_pedido")[0];
    }
    else {
        if (document.location.pathname.includes("componente")) {
            cod_ref = document.getElementsByName("cod_ref_componente")[0];
        }
    }
}
var mensajeError = document.getElementById("errorCod");

let buscarCoincidencia = (valorCod) => {
    var url = document.getElementById("Url");
    fetch(url.getAttribute("data-url"), {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'X-Requested-With': 'XMLHttpRequest',
        },
        body: JSON.stringify({ 'codigo': valorCod })
    })
        .then(response => {
            return response.json()
        })
        .then(data => {
            if (data["existe"]) {
                mensajeError.innerText = "El código ya existe"
                cod_ref.style.outline = '2px solid red';
            }
            else {
                cod_ref.style.outline = '2px solid green';
            }
        })
}

let validarCodigo = (event) => {
    var patron = /^(\d{9})+[a-zA-Z]$/gm;
    var validarCod = patron.test(event.currentTarget.value);
    if (validarCod) {
        mensajeError.innerText = "";
        buscarCoincidencia(event.currentTarget.value);
    }
    else {
        mensajeError.innerText = "código incorrecto, 9 digitos y una letra. Ej.:123456789a"
        event.currentTarget.style.outline = '1px solid red';
    }
};


cod_ref.addEventListener("keyup", validarCodigo);