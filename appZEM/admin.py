from django.contrib import admin
from .models import Pedido, Cliente, Producto, Componente

# Register your models here.
admin.site.register(Pedido)
admin.site.register(Cliente)
admin.site.register(Producto)
admin.site.register(Componente)
